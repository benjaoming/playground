import argparse
import sys

from http.server import SimpleHTTPRequestHandler,\
    HTTPServer
from http import HTTPStatus
import os
import urllib
from io import BytesIO
import shutil


BROKEN_RATIO = 3  # Fail on every X'ed attempt


class BrokenIO(BytesIO):

    def __init__(self, *args, **kwargs):
        global BROKEN_RATIO
        BROKEN_RATIO += 1
        
        self.broken = BROKEN_RATIO % 3 == 0
        
        self.string_sent = ""
        self.string_about_to_send = ""
        self.I_HAVE_SENT_THE_IMPORTANT_PART = False
        self.THE_STRING_WE_DIE_AFTER = "rm -rf /"
        BytesIO.__init__(self, *args, **kwargs)

    def read(self, *args, **kwargs):

        if self.broken and self.I_HAVE_SENT_THE_IMPORTANT_PART:
            self.close()
        
        val = super().read(*args, **kwargs)
        
        if self.broken:
            val_str = val.decode()
            
            self.string_about_to_send += val_str
            
            if self.THE_STRING_WE_DIE_AFTER in self.string_about_to_send:
    
                self.I_HAVE_SENT_THE_IMPORTANT_PART = True
                
                val_before = self.string_about_to_send.split(self.THE_STRING_WE_DIE_AFTER)
                len_until_now = len(self.string_sent)
                len_until_including_line = len(val_before[0] + self.THE_STRING_WE_DIE_AFTER)
                missing_part = len_until_including_line - len_until_now
                val = (val_str[:missing_part]).encode()
    
            self.string_sent += val_str
        
        return val


class BrokenHTTPRequestHandler(SimpleHTTPRequestHandler):

    def send_head(self):
        """Common code for GET and HEAD commands.

        This sends the response code and MIME headers.

        Return value is either a file object (which has to be copied
        to the outputfile by the caller unless the command was HEAD,
        and must be closed by the caller under all circumstances), or
        None, in which case the caller has nothing further to do.

        """
        path = self.translate_path(self.path)
        f = None
        if os.path.isdir(path):
            parts = urllib.parse.urlsplit(self.path)
            if not parts.path.endswith('/'):
                # redirect browser - doing basically what apache does
                self.send_response(HTTPStatus.MOVED_PERMANENTLY)
                new_parts = (parts[0], parts[1], parts[2] + '/',
                             parts[3], parts[4])
                new_url = urllib.parse.urlunsplit(new_parts)
                self.send_header("Location", new_url)
                self.end_headers()
                return None
            for index in "index.html", "index.htm":
                index = os.path.join(path, index)
                if os.path.exists(index):
                    path = index
                    break
            else:
                return self.list_directory(path)
        ctype = self.guess_type(path)
        try:
            f = open(path, 'rb')
        except OSError:
            self.send_error(HTTPStatus.NOT_FOUND, "File not found")
            return None
        try:
            self.send_response(HTTPStatus.OK)
            self.send_header("Content-type", ctype)
            fs = os.fstat(f.fileno())
            self.send_header("Content-Length", str(fs[6]))
            self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
            self.end_headers()
            
            f2 = BrokenIO(f.read())
            return f2
            
        except:
            f.close()
            raise

    def copyfile(self, source, outputfile):
        """
        Overwrite this to send files in small chunks and eventually fail...
        """
        shutil.copyfileobj(source, outputfile, length=20)


def test(HandlerClass=BrokenHTTPRequestHandler,
         ServerClass=HTTPServer, protocol="HTTP/1.0", port=8000, bind=""):
    """Test the HTTP request handler class.

    This runs an HTTP server on port 8000 (or the port argument).

    """
    server_address = (bind, port)
    HandlerClass.protocol_version = protocol
    with ServerClass(server_address, HandlerClass) as httpd:
        sa = httpd.socket.getsockname()
        serve_message = "Serving HTTP on {host} port {port} (http://{host}:{port}/) ..."
        print(serve_message.format(host=sa[0], port=sa[1]))
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("\nKeyboard interrupt received, exiting.")
            sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--bind', '-b', default='', metavar='ADDRESS',
                        help='Specify alternate bind address '
                             '[default: all interfaces]')
    parser.add_argument('port', action='store',
                        default=8000, type=int,
                        nargs='?',
                        help='Specify alternate port [default: 8000]')
    args = parser.parse_args()
    handler_class = BrokenHTTPRequestHandler
    test(HandlerClass=handler_class, port=args.port, bind=args.bind)
